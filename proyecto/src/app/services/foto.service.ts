import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FotoService {

  constructor(private http: HttpClient) { }

  getAllPictures(){
    return this.http.get('https://picsum.photos/v2/list');
  }
}

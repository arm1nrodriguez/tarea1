import { Component, OnInit } from '@angular/core';
import {FotoService} from "../../services/foto.service";

@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.component.html',
  styleUrls: ['./fotos.component.scss']
})
export class FotosComponent implements OnInit {

  data: any;

  constructor(private fotoService: FotoService) { }

  ngOnInit(): void {
   this.data = this.fotoService.getAllPictures();
  }

}
